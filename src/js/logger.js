import html2canvas from 'html2canvas';

(function (root, factory) {
  if (typeof define === "function" && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports === "object") {
    module.exports = factory(require("jquery"));
  } else {
    root.Requester = factory(root.jQuery);
  }
}(this, function (jQuery) {

  var Logger = function() {


    // Define option defaults
    var defaults = {

    };

    // Create options by extending defaults with the passed in arugments
    if (arguments[0] && typeof arguments[0] === "object") {
      this.options = extendDefaults(defaults, arguments[0]);
    }



    var errorHandler = curryErrorHandler.call(this, this.options.url);
    window.onerror = errorHandler;

  };

  function curryErrorHandler(sendUrl) {
    return function (message, url, line, col, error) {
      console.log("Error sent");
      html2canvas(document.body).then(function(canvas) {
        var img = canvas.toDataURL();

        var err = {
          "message": message,
          "url": url,
          "line": line,
          "image": img
        };

        var req =  new XMLHttpRequest();
        req.open('POST', sendUrl, true);
        req.setRequestHeader("Content-type", "application/json");
        req.send(JSON.stringify(err));

      });
      return false;
    }
  }

  //Private method
  Logger.prototype.func = function() {
    console.log('func');
  };



  function extendDefaults(source, properties) {
    var property;
    for (property in properties) {
      if (properties.hasOwnProperty(property)) {
        source[property] = properties[property];
      }
    }
    return source;
  }

  return Logger;

}));