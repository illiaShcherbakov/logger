var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var app = express();


app.use(cors());
app.use(bodyParser());

app.post('/', function (req, res) {
  console.log('body: ' + JSON.stringify(req.body));
  res.send(req.body);
});

app.listen(4000, function () {
  console.log('Example app listening on port 4000!')
});